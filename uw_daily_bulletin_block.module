<?php

/**
 * @file
 * The daily bulletin block module.
 */

/**
 * Implements hook_block_info().
 */
function uw_daily_bulletin_block_block_info() {
  $blocks = array();
  $blocks['uw-daily-bulletin'] = array(
    'info' => t('Waterloo Daily Bulletin'),
    'cache' => DRUPAL_CACHE_GLOBAL,
  );
  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function uw_daily_bulletin_block_block_view($delta = '') {
  $block = array();
  $subject = NULL;
  $content = NULL;

  switch ($delta) {
    case 'uw-daily-bulletin':

      // Get and check the cache
      // cage_get returns FALSE if there is nothing.
      $cache = cache_get('uw-daily-bulletin-block-content', 'cache');

      if (isset($cache->data)) {
        // Get bulletin date from cache.
        $cached_bulletin_data = $cache->data;
        preg_match('/<h3><a(.*)>(.*)<\/a><\/h3>/', $cached_bulletin_data, $cached_bulletin_date_array);
        $cached_bulletin_date = $cached_bulletin_date_array[2];

        // Get todays date and time to check if cache needs to be reloaded.
        $todays_date = date('l, F j, Y');
        $current_time = date('Hi');

        // Get weekday for ensuring that we are only reloading cache on a work day.
        $weekday = date("w", strtotime($todays_date));

        // Set the bulletin start and stop time.
        $bulletin_start_reload_time = '0845';
        $bulletin_stop_reload_time = '0930';

        $force_reload_cache = FALSE;
        // Check if time is between the publishing time for the bulletin.
        if ($current_time >= $bulletin_start_reload_time && $current_time <= $bulletin_stop_reload_time) {
          // Check if the old bulletin is still in cache and it is a weekday,
          // if so going to force a reload of the bulletin.
          if ($cached_bulletin_date !== $todays_date && $weekday >= 1 && $weekday <= 5) {
            $force_reload_cache = TRUE;
          }
        }
      }

      // No cache or it has expired, get the data gain.
      if (!$cache || $cache->expire <= time() || $force_reload_cache) {

        // "@" in front prevents visible warning if file fails to load.
        $xml = @simplexml_load_file('https://uwaterloo.ca/daily-bulletin/index.xml');
        // Generate an empty array outside the if statement so we still get a block when the feed doesn't load.
        $daily_bulletin_data = array();

        if ($xml) {

          // Extract date.
          $daily_bulletin_data['date'] = check_plain(trim((string) $xml->channel->item->title));

          // Extract link.
          $daily_bulletin_data['link'] = check_plain(trim((string) $xml->channel->item->guid));

          // Extract headlines
          // the headlines are enclosed in CDATA, so we extract this data and convert it into
          // a simpleXML object so we can extract the headlines.
          $description = (trim((string) $xml->channel->item->description));
          $dom = new DOMDocument();
          $success = @$dom->loadHTML($description);
          $description_xml = simplexml_import_dom($dom);
          $headline_summary = $description_xml->xpath("//div[@class='headline_summary']/ul/li");

          // Extract each headline into an array for rendering.
          $headlines = array();
          foreach ($headline_summary as $headline) {
            $headline_parts = array();
            foreach ($headline->strong as $headline_part) {
              $headline_parts[] = (string) $headline_part;
            }
            $headline = implode(' ', $headline_parts);
            $headlines[] = check_plain(trim($headline));
          }

          $daily_bulletin_data['headlines'] = $headlines;
        }

        $content = theme('uw_daily_bulletin_block_template', $daily_bulletin_data);

        // Set the cache, expires in 15 minutes
        // note: if reading the $xml file failed (problem reading feed), $content is empty and only a basic block will be rendered
        // still set the cache (even though $content is null) so that we aren't trying to constantly load a bad feed.
        // Cache for 15 minutes.
        cache_set('uw-daily-bulletin-block-content', $content, 'cache', time() + 900);
      }
      else {
        $content = $cache->data;
      }

      break;
  }

  $block['subject'] = $subject;
  $block['content'] = $content;
  return $block;
}

/**
 * Implements hook_theme().
 */
function uw_daily_bulletin_block_theme() {
  $themes = array(
    'uw_daily_bulletin_block_template' => array(
      'template' => 'daily-bulletin-block',
      'variables' => array('date' => NULL, 'link' => NULL, 'headlines' => NULL),
    ),
  );
  return $themes;
}
