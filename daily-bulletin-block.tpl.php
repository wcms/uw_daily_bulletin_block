<?php

/**
 * @file
 * The daily bulletin block template.
 */

/**
 * Available variables:
 *  $date
 *  $link
 *  $headlines (array)
 */
?>
<div id="daily-bulletin">
  <h2 class="black">Daily Bulletin</h2>
  <div class="bulletin-headlines">
    <?php
    if (!empty($date) && !empty($link)) {
    ?>
    <h3><?php print l($date, $link); ?></h3>
    <?php
    }
      if (!empty($headlines)) {
        $list = array(
          'items' => $headlines,
          'title' => NULL,
          'type' => 'ul',
        );
        print theme('item_list', $list);
      }
    ?>
  </div>
  <p class="morelink"><a href="https://uwaterloo.ca/daily-bulletin">See the Daily Bulletin &raquo;</a></p>
</div>
